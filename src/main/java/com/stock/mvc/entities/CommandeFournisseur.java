package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandeFournisseur implements Serializable {

	@Id
	@GeneratedValue
	private Long idCommandeFourinsseur;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datecommande;
	
	@ManyToOne
	@JoinColumn(name= "idFournisseur")
	private Fournisseur fournisseur;
	
	@OneToMany(mappedBy="commandeFournisseur")
	private List<LigneCommandeFournisseur> LigneCommandeFournisseurs;
	
	public Long getIdCommandeFourinsseur() {
		return idCommandeFourinsseur;
	}

	public void setId(long id) {
		this.idCommandeFourinsseur = id;
	}

	public Date getDatecommande() {
		return datecommande;
	}

	public void setDatecommande(Date datecommande) {
		this.datecommande = datecommande;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public List<LigneCommandeFournisseur> getLigneCommandeFournisseurs() {
		return LigneCommandeFournisseurs;
	}

	public void setLigneCommandeFournisseurs(List<LigneCommandeFournisseur> ligneCommandeFournisseurs) {
		LigneCommandeFournisseurs = ligneCommandeFournisseurs;
	}

	public void setIdCommandeFourinsseur(Long idCommandeFourinsseur) {
		this.idCommandeFourinsseur = idCommandeFourinsseur;
	}
	
	
	
}
