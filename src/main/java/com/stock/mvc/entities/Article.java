package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="article")
public class Article implements Serializable{

	@Id
	@GeneratedValue
	private long idArticle;
	
private String codeArticle;

private String designation;

private BigDecimal prixUnitaire;

private BigDecimal tauxTva;

private BigDecimal prixUnitaureTtc;

private String photo;

public Article() {
}

//plusieurs article appartient a une catégorie(dans une cat o peut avoir +ieurs article)
@ManyToOne
@JoinColumn(name= "idCtegory") //idCategory de table category
private Category category;

	public long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(long idArticle) {
		this.idArticle = idArticle;
	}

	public String getCodeArticle() {
		return codeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(BigDecimal prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixUnitaureTtc() {
		return prixUnitaureTtc;
	}

	public void setPrixUnitaureTtc(BigDecimal prixUnitaureTtc) {
		this.prixUnitaureTtc = prixUnitaureTtc;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	
}
