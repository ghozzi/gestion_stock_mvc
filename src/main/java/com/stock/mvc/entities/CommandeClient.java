package com.stock.mvc.entities;

import java.io.Serializable;
import java.security.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandeClient implements Serializable {

	@Id
	@GeneratedValue
	private Long idCommandeClient;

	private String code;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datecommande;
	
	@ManyToOne
	@JoinColumn(name= "idClient")
	private Client client;
	
	@OneToMany(mappedBy="commandeClient")
	private List<LigneCommandeClient> LigneCommandeClients;
	public Long getIdCommandeClient() {
		return idCommandeClient;
	}

	public void setId(long id) {
		this.idCommandeClient = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDatecommande() {
		return datecommande;
	}

	public void setDatecommande(Date datecommande) {
		this.datecommande = datecommande;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<LigneCommandeClient> getLigneCommandeClient() {
		return LigneCommandeClients;
	}

	public void setLigneCommandeClient(List<LigneCommandeClient> ligneCommandeClient) {
		LigneCommandeClients = ligneCommandeClient;
	}

	public void setIdCommandeClient(Long idCommandeClient) {
		this.idCommandeClient = idCommandeClient;
	}

	public List<LigneCommandeClient> getLigneCommandeClients() {
		return LigneCommandeClients;
	}

	public void setLigneCommandeClients(List<LigneCommandeClient> ligneCommandeClients) {
		LigneCommandeClients = ligneCommandeClients;
	}
	
	
	
}
